/**
 * Created by tthlex on 11/10/15.
 */

var yamljs = require('yamljs');
var path = require('path');

function getConfig(env) {
    console.log(process.env.NODE_ENV);
    var env_file = path.resolve(__dirname, "config." + env + ".yml");
    return yamljs.load(env_file);
}
module.exports = getConfig(process.env.NODE_ENV);