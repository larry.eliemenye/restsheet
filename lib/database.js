var mongoose = require('mongoose');
var config = require('../config');
var connectionString = config.database.url;

mongoose.connect(connectionString);
dbcon = mongoose.connection;

dbcon.on('error', function (error) {
    console.log('Error connecting Mongo DB: ' + error.message);
});
mongoose.set('debug', true);
module.exports = mongoose;