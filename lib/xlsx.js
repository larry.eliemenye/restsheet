/**
 * Created by tthlex on 11/10/15.
 */


//this will analyse the xsls file and spit out a huge JSON structure representing its Data. you read from this JSON structure;
//We a need to query language to read XLSX data
var spreedsheet = require('xlsx');
var SpahQl = require('spahql');
var _ = require('lodash');

/**
 *
 * @name SpreadSheet
 * @description Constructs a spread sheet opbject which lets you manipulate and get spreadsheet data
 * @param {string} filepath
 * @param {object} options
 *
 * */
function SpreadSheet(file, options){}

/**
 *
 *@name readFile
 *@description reads in an excel file for manipulation
 *
 * */
function readFile(file) {}

/**
 *
 * get columns with data
 *
 * */
function getColumns() {};

/**
 *
 * gets rows with data
 *
 * */
function getRows() {};

/**
 *
 * gets worksheet data
 *
 * */
function getWorkSheets() {};

/**
 *
 * Pass in cells and get back containing data
 *
 * */
function getGetCell(cells) {}

/**
 *
 * Find a string in sheet
 *
 * */
function find(worksheet) {}


exports.middleware = function(options){
    return function(req, res, next){}
};

