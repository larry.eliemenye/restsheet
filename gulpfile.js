/**
 * Created by tthlex on 11/10/15.
 */

var gulp = require('gulp');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
var copy = require('gulp-contrib-copy');
var config = require('./config');


/**
 * Dev tasks
 * */

gulp.task("default", ['build:frontend'],function(){
    gulp.watch("ngApp/**/*.*", ['compile:script', 'copy:partials']);
});

gulp.task('build:frontend', ['compile:script', 'copy:partials']);
gulp.task('compile:script', function () {
    gulp.src('./ngApp/index.js')
        .pipe(browserify({
            insertGlobals: true
        }))
        .pipe(gulp.dest('./public/build'));
});
gulp.task('build:frontend', ['compile:script', 'copy:partials']);

gulp.task('copy:partials', function () {
    gulp
        .src('./ngApp/partials/*.html')
        .pipe(gulp.dest('./public/build/partials'))
});


//TODO:build env specific config for angular app
gulp.task('build:env-config',function(){});

/**
 *
 * Deploy tasks
 *
 * */