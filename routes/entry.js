var express = require('express');
var router = express.Router();
var EntryModel = require('../model/Entry');

router.route('/entry')
    .get(getEntry)
    .post(AddEntry)
    .put(AddEntry)
    .delete(deleteEntry);

router.route('/:id')
    .get(getEntry)
    .post(AddEntry)
    .put(AddEntry)
    .delete(deleteEntry);

function getEntry(req, res, next){
    res.json([{
        name:"AppTranslations API",
        baseUrl:"http://api.restsheet.com/methra/app-translations/",
        description:"Exposes translation strings to the client"
    },{
        name:"Users API",
        baseUrl:"http://api.restsheet.com/methra/users/",
        description:"Exposes some sample users"
    },{
        name:"Shopping Lists API",
        baseUrl:"http://api.restsheet.com/methra/shoppinglist/",
        description:"Exposes shopping lists to app"
    },{
        name:"Contacts API",
        baseUrl:"http://api.restsheet.com/methra/contacts/",
        description:"Just exposes some dummy contacts"
    }])
}
function AddEntry(){}
function deleteEntry(){}

module.exports = router;
