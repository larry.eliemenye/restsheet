var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('pages/landing', { title: 'Express' });
});

/* GET App page */
router.get('/main', function(req, res, next) {
  res.render('pages/app');
});

/* GET home page. */
router.get('/signin', function(req, res, next) {
  res.redirect('/main');
});

/* GET App page */
router.get('/signup', function(req, res, next) {
  res.render('pages/app');
});

module.exports = router;
