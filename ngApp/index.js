/**
 * Created by tthlex on 12/10/15.
 */


var config =                require('./config');
var mainController =        require('./controllers/mainController');
var entryListDirective =    require('./directives/entryList');
var entryService =          require('./services/EntryService');
var configuration =         require('./constants/configuration');

//main angular module
var RestSheet = angular.module("RestSheet", ['ui.router', 'ngFileUpload']);

//config
RestSheet.config(config);

//constants
RestSheet.constant('ConfigValues', configuration);
RestSheet.config(function(ConfigValues){
    console.table(ConfigValues);
});

//controllers
RestSheet.controller("MainController", mainController);

//services
RestSheet.service('EntryService', entryService);

//directives
RestSheet.directive("entryList", entryListDirective);