/**
 * Created by tthlex on 12/10/15.
 */

var mainController = require('./controllers/mainController');
module.exports = function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/index');
    $stateProvider
        .state('index', {
            url: '/index',
            templateUrl: '/build/partials/index.tmpl.html',
            controller:mainController
        })
        .state('settings', {
            url: '/settings',
            templatesUrl: '/build/partials/settings.tmpl.html'
        }).state('api-console', {
            url: '/api-console',
            templateUrl: '/build/partials/api-console.tmpl.html'
        })
};