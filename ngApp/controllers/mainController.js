/**
 * Created by tthlex on 12/10/15.
 */
var dependencies = "$scope, Upload";
module.exports = [ dependencies, function MainControllerFn($scope){
    $scope.entries = [{
        name:"AppTranslations",
        baseUrl:"http://api.restsheet.com/methra/app-translations/",
        description:"These endpoints expose translation strings to the client"
    },{
        name:"Users",
        baseUrl:"http://api.restsheet.com/methra/users/",
        description:"Just exposes some sample users"
    }]
}];