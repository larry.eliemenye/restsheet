/**
 * Created by tthlex on 13/10/15.
 */


var linkFn = function(EntryService, $scope, el, attrib, ctrl){
    var list = el;
    $scope.entries = [];

    EntryService.getEntrySettings();
    $scope.toggle = function(evt){
        var orginalTarget = angular.element(evt.target);
        var currentTarget = angular.element(evt.currentTarget);

        //do not propergate further if click event if not from the intended node
        if(orginalTarget[0].id !== "collapser") return false;

        //switch class
        if(currentTarget.hasClass("entry-item") || currentTarget.hasClass("entry-item entry-item-expanded")){
            currentTarget.toggleClass("entry-item-expanded");
            return false
        }
    }
};


var dependencies = 'EntryService';
module.exports = [dependencies, function(EntryService){

    console.log(EntryService);
    return{
        restrict:"A",
        templateUrl:'/build/partials/entry-list.tmpl.html',
        link:linkFn.bind(null, EntryService)
    }
}];