/**
 * Created by tthlex on 11/10/15.
 */
var mongoose = require('../../lib/database');
var MongooseSchema = mongoose.Schema;

var EntryModel = new MongooseSchema({
    username:String,
    hash:String,
    email:String
});
module.exports = mongoose.model('Client', EntryModel);