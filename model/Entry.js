/**
 * Created by tthlex on 26/06/15.
 */

var mongoose = require('../lib/database');
var MongooseSchema = mongoose.Schema;

var EntryModel = new MongooseSchema({
    entryName:String,
    entryDescription:String
});
module.exports = mongoose.model('Entry', EntryModel);